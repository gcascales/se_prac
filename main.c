//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"
#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"
#include <grlib.h>
#include "Crystalfontz128x128_ST7735.h"

// Definicion de parametros RTOS
#define READER_TASK_PRIORITY 4
#define WRITER_TASK_PRIORITY 3
#define WRITERA_TASK_PRIORITY 2
#define HEARTBEAT_TASK_PRIORITY 1
#define QUEUE_SIZE  10
#define HALF_SECOND_MS  500
#define HEART_BEAT_ON_MS 10
#define HEART_BEAT_OFF_MS 990
#define BUFFER_SIZE 180 //Necesario para almacenar 60 valores de temperatura (y luego tambi�n 120 de luz, aunque esto no haya falta realmente...)
#define NUM_ADC_CHANNELS 3
#define SENDER_PERIOD_MS    100
#define RECEIVER_PERIOD_MS    60000

#define DEBUG_MSG 0

// Prototipos de funciones privadas
static void prvSetupHardware(void);
static void prvTempLightWriterTask(void *pvParameters);
static void prvAccelWriterTask(void *pvParameters);
static void prvReaderTask(void *pvParameters);
static void prvHeartBeatTask(void *pvParameters);

static TickType_t sender_delay;
static TickType_t receiver_delay;

// Declaracion de un semaforo binario para la ISR/lectura de buffer (Acceler�metro)
volatile SemaphoreHandle_t xBinarySemaphore;

//VARIALES SENSORES LUZ Y TEMPERATURA
typedef enum Sensor
{
    light = 1,
    temp = 2
} Sensor;
// Queue element
typedef struct {
    Sensor sensor;
    float value;
} Queue_reg_t;
// Buffer de mensajes
Queue_reg_t buffer[BUFFER_SIZE];
uint8_t buff_pos;
float laverage = 0; //Valor para la media de luz
float taverage = 0; //Valor para la media de temperaruta
float taverage_last = 0; //Valor anterior de temperatura
int minutes = 0; //Va contando los minutos

//VARIABLES ACCELEROMETRO
//int acount = 0; //Contador para saber cuando hemos obtenido 10 muestras de aceleraci�n
float aaveragex = 0; //Valor para la media de aceleraci�n (X)
float aaveragey = 0; //Valor para la media de aceleraci�n (Y)
float aaveragez = 0; //Valor para la media de aceleraci�n (Z)
// Declaracion de una cola de mensajes
QueueHandle_t xQueue;
// Tipo de mensaje a almacenar en la cola
typedef struct {
    float acc_x;
    float acc_y;
    float acc_z;
} acc_t;

//VARIABLE PARA EL DISPLAY LCD
/* Graphic library context */
Graphics_Context g_sContext;


//funcion para calcular la media de los valores. El modo indica 1=Temperatura, 2=Luz, 3=Aceleraci�n
void average(int mode){
    uint8_t buff_pos_aux;//Variable auxiliar para la posici�n del buffer (para la luz)
    acc_t queue_element;
    taverage_last = taverage;
    int laverage_count = 0; //Para contar las muestras de luz que tenemos
    int laverage_index = 0; //Indice del buffer de luz y temperatura, para luz
    buff_pos_aux = buff_pos; //nos creamos una variable local para saber donde estamos situados en el buffer de Luz y Temperatura
    switch (mode){
    case 1: //Calculamos la media de la temperatura utilizando las �ltimas 60 muestras (todo el buffer)
        taverage = 0;
        for(int i=1; i<BUFFER_SIZE; i++){//empezamos con el campo 1, que ser� el primero de temperatura, y luego incrementamos
            if (buffer[i].sensor == temp){ //Miramos si tenemos una medida de temperatura
                taverage = taverage + buffer[i].value;
            }
        }
        taverage = taverage / 60;
        break;
    case 2://Calculamos la media de la luz utilizando las �ltimas 2 muestras.
        laverage = 0;
        while (laverage_count < 2){//Obtenemos muestras de luz hasta tener las 2 muestras deseadas
            if (buffer[(buff_pos_aux - laverage_index)  % BUFFER_SIZE].sensor == light){//Si la lectura es de luz, la utilizamos
                laverage = laverage + buffer[(buff_pos_aux - laverage_index) % BUFFER_SIZE].value;
                laverage_count++;
            }
            laverage_index++;
        }
        laverage = laverage / 2;
        break;
    case 3://Calculamos la media del aceler�metro utilizando las �ltimas 10 muestras.
        aaveragex = 0;
        aaveragey = 0;
        aaveragez = 0;
        for(int i=1; i<=10; i++){
              xQueueReceive( xQueue, &queue_element, portMAX_DELAY );
              aaveragex = aaveragex + queue_element.acc_x;
              aaveragey = aaveragey + queue_element.acc_y;
              aaveragez = aaveragez + queue_element.acc_z;
        }
        aaveragex = aaveragex / 10;
        aaveragey = aaveragey / 10;
        aaveragez = aaveragez / 10;
        break;
    }
}

void draw(char *message, uint32_t posY)
{
    //Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext,
                                    (int8_t *)message,
                                    AUTO_STRING_LENGTH,
                                    64,
                                    posY,
                                    OPAQUE_TEXT);
}

int main(void)
{
    // Inicializacion del semaforo binario (Accelerometro)
    xBinarySemaphore = xSemaphoreCreateBinary();
    // Inicializacion de la cola (Accelerometro)
    xQueue = xQueueCreate( QUEUE_SIZE, sizeof( acc_t ) );

    sender_delay = pdMS_TO_TICKS( SENDER_PERIOD_MS );
    receiver_delay = pdMS_TO_TICKS( RECEIVER_PERIOD_MS );

    // Comprueba si semafor y cola se han creado bien
    if ((xBinarySemaphore != NULL)&& (xQueue != NULL))
        {
        // Inicializacion del hardware (clocks, GPIOs, IRQs)
        prvSetupHardware();
        // Inicializacion buffer
        for(int i=0; i<BUFFER_SIZE;i++){
            buffer[i].sensor = light;
            buffer[i].value = -1.0;
        }
        buff_pos = 0;

        // Creacion de tareas
        xTaskCreate( prvTempLightWriterTask, "prvTempLightWriterTask", configMINIMAL_STACK_SIZE, NULL,
                     WRITER_TASK_PRIORITY,NULL );
        xTaskCreate( prvAccelWriterTask, "AccelWriterTask", configMINIMAL_STACK_SIZE, NULL,
                    WRITER_TASK_PRIORITY, NULL );
        xTaskCreate( prvReaderTask, "ReaderTask", 200, NULL,
                    READER_TASK_PRIORITY, NULL );//La medida de STACK ha de ser 200, con 100 no funciona
        xTaskCreate( prvHeartBeatTask, "HeartBeatTask", configMINIMAL_STACK_SIZE, NULL,
                     HEARTBEAT_TASK_PRIORITY, NULL );
        // Puesta en marcha de las tareas creadas
        vTaskStartScheduler();
    }
    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema
static void prvSetupHardware(void){

    extern void FPU_enableModule(void);

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();
    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_6);
    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    /* Initializes display */
    Crystalfontz128x128_Init();

    /* Set default screen orientation */
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);

    /* Initializes graphics context */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();
    // Inicializacion del sensor TMP006
    TMP006_init();
    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    // Reset del flag de interrupcion del pin P1.1
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN1);
    // Habilita la interrupcion del pin P1.1
    MAP_GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN1);
    // Configura la prioridad de la interrupcion del PORT1
    MAP_Interrupt_setPriority(INT_PORT1, 0xA0);
    // Habilita la interrupcion del PORT1
    MAP_Interrupt_enableInterrupt(INT_PORT1);
    // Inicializa acelerometro+ADC
    init_Accel();
    // Habilita que el procesador responda a las interrupciones
    MAP_Interrupt_enableMaster();
}

//Tarea heart beat
static void prvHeartBeatTask (void *pvParameters){
    for(;;){
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_ON_MS) );
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_OFF_MS) );
    }
}

//Tarea lectura temperatura y luz
static void prvTempLightWriterTask (void *pvParameters){
    // Resultado del envio a la cola
    Queue_reg_t queueRegister;
    int second = 0;
    float temperature;
    uint16_t rawData;
    float convertedLux;

    for(;;){
        vTaskDelay( pdMS_TO_TICKS(HALF_SECOND_MS) );
        if (second == 1){//Si ha pasado un segundo, leemos temperatura
            // Lee el valor de la medida de temperatura
            temperature = TMP006_readAmbientTemperature();

            queueRegister.sensor = temp;
            queueRegister.value = temperature;
            // Envia un comando a traves de la cola si hay espacio
            buff_pos++;
            buffer[buff_pos % BUFFER_SIZE] = queueRegister;
            second = 0;
        }else{ //Si no ha pasado, es que ha pasado medio segundo. En la siguiente vuelta tendremos, pues, el segundo completo
            second = 1;
        }

        // Lee el valor de la medida de luz
        sensorOpt3001Read(&rawData);
        sensorOpt3001Convert(rawData, &convertedLux);

        queueRegister.sensor = light;
        queueRegister.value = convertedLux;
        // Envia un comando a traves de la cola si hay espacio
        buff_pos++;
        buffer[buff_pos % BUFFER_SIZE] = queueRegister;

    }
}

//Tarea lectura accelerometro
static void prvAccelWriterTask (void *pvParameters){
    // Resultado del envio a la cola
    acc_t queue_element;
    float Datos[NUM_ADC_CHANNELS];
    xSemaphoreTake( xBinarySemaphore, 0 );
    // La tarea se repite en un bucle infinito
    for(;;){
        Accel_read(Datos);
        queue_element.acc_x = Datos[0];
        queue_element.acc_y = Datos[1];
        queue_element.acc_z = Datos[2];
        xQueueSend(xQueue, &queue_element, portMAX_DELAY);
        vTaskDelay(sender_delay);
    }
}

//Tarea lectura de cola
static void prvReaderTask (void *pvParameters)
{
    char message[100];
    char value[10];
    for(;;){
        Graphics_clearDisplay(&g_sContext);
        if (minutes == 0){//Si no ha pasado ni 1 minuto, no tendremos nada que mostrar
            strcpy (message,"INICIALIZANDO");
            draw (message,5);
            minutes++;
        }else{
            //Mostramos valores de Luz y Temperatura
            strcpy(message, "LUZ: ");
            average(2);
            ftoa(laverage, value, 2);
            strcat(message,value);
            draw(message,20);
            strcpy(message,"TEMPERATURA: ");
            average(1);
            ftoa(taverage, value, 2);
            strcat(message,value);
            draw(message,35);
            //Para mostrar la diferencia de temperatura, m�nimo tendr�n que haber pasado 2 minutos.
            if (minutes==1){
                  draw("DIF.TEMP: N/S",50);
                  minutes++;
            }else{
                  strcpy(message,"DIF.TEMP: ");
                  ftoaA((taverage - taverage_last), value, 2);
                  strcat(message,value);
                  draw(message,50);
            }

            strcat(message,"\n\r");

            //Mostramos los valores medios del Accelerometro
            draw("ACELEROMETRO:",65);
            average(3);
            strcpy(message,"X: ");
            ftoaA(aaveragex, value, 2); strcat(message,value);
            draw(message,80);
            strcpy(message,"Y: ");
            ftoaA(aaveragey, value, 2); strcat(message,value);
            draw(message,95);
            strcpy(message,"Z: ");
            ftoaA(aaveragez, value, 2); strcat(message,value);
            draw(message,110);
        }
        vTaskDelay(receiver_delay);
    }
}

